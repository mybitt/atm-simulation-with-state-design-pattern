/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author abenezer
 */
public class NoCash implements ATMState{
    
    private final ATMMachine atmMachine;

    public NoCash(ATMMachine aTMMachine) {
        this.atmMachine = aTMMachine;
    }
 
    @Override
    public void insertCard() {
        
        System.out.println("We don't have money...");
    }

    @Override
    public void ejectcard() {
        
        System.out.println("We don't have money. You didn't enter a card");
    }

    @Override
    public void insertPin(int pinEntered) {
        System.out.println("We don't have money");
    }

    @Override
    public void requestCash(int cashToWithDraw) {
        
        System.out.println("We don't have money");
    }
    
    
}