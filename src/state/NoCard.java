/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author abenezer
 */
public class NoCard implements ATMState{

    private ATMMachine atmMachine;

    public NoCard(ATMMachine aTMMachine) {
        this.atmMachine = aTMMachine;
    }
    
    @Override
    public void insertCard() {
        System.out.println("Please Enter PIN");
        atmMachine.setATMState(atmMachine.getYesCardState());
     }

    @Override
    public void ejectcard() {
        
        System.out.println("Insert a card first");
     }

    @Override
    public void insertPin(int pinEntered) {
        System.out.println("Insert a card first");
    }

    @Override
    public void requestCash(int cashToWithDraw) {
        
        System.out.println("Insert a card first");
    }
    
}
