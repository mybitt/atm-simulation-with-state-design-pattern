/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author elnatal
 */
public class HasCard implements ATMState{

    private final ATMMachine atmMachine;

    public HasCard(ATMMachine aTMMachine) {
        
        this.atmMachine = aTMMachine;
    }
    
    
    @Override
    public void insertCard() {
        
        System.out.println("You cannot enter any other card");
    }

    @Override
    public void ejectcard() {
        System.out.println("Card Ejected");
        atmMachine.setATMState(atmMachine.getNoCardState());
    }

    @Override
    public void insertPin(int pinEntered) {
        
        if(pinEntered == 1234)
        {
            System.out.println("Correct Pin");
            atmMachine.correctPinEntered = true;
            atmMachine.setATMState(atmMachine.getHasPin());
        }
        else
        {
            System.out.println("Wrong Pin");
            atmMachine.correctPinEntered = false;
            System.out.println("Card Ejected");
            atmMachine.setATMState(atmMachine.getNoCardState());
        }
     }

    @Override
    public void requestCash(int cashToWithDraw) {
        
        System.out.println("Enter Pin first");
    }
    
}
