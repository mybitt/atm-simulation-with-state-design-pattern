/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author abenezer
 */
public interface ATMState {
    
    public void insertCard();
    public void ejectcard();
    public void insertPin(int pinEntered);
    public void requestCash(int cashToWithDraw);
    
}
