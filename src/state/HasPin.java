/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author abenezer
 */
public class HasPin implements ATMState{
    
    private final ATMMachine atmMachine;

    public HasPin(ATMMachine aTMMachine) {
        this.atmMachine = aTMMachine;
    }

    @Override
    public void insertCard() {
         System.out.println("You cannot enter any other card");
    }

    @Override
    public void ejectcard() {
        
        System.out.println("Card Ejected");
        atmMachine.setATMState(atmMachine.getNoCardState());
    }

    @Override
    public void insertPin(int pinEntered) {
        
        System.out.println("Already PIN is entered");
    }

    @Override
    public void requestCash(int cashToWithDraw) {
        
        if(cashToWithDraw>atmMachine.cashInMachine)
        {
            System.out.println("No allowed to with draw that much amount");
            System.out.println("Card Ejected");
            atmMachine.setATMState(atmMachine.getNoCardState());
        }
        else
        {
            System.out.println(cashToWithDraw+" is despansed by the machine");
            atmMachine.setCashInMachine(atmMachine.cashInMachine-cashToWithDraw);
            System.out.println("Card Ejected");
            atmMachine.setATMState(atmMachine.getNoCardState());
            
            if(atmMachine.cashInMachine<=0)
            {
              atmMachine.setATMState(atmMachine.getNoCardState());
              
            }
        }
        
    }
    
}
