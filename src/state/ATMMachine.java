/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author abenezer
 */
public class ATMMachine {
    
    ATMState atmState;
    ATMState hasCard;
    ATMState noCard;
    ATMState hasCorrectPin;
    ATMState atmOutOfMoney;
    
    int cashInMachine = 20000;
    
    boolean correctPinEntered = false;

    public ATMMachine() {
        
        hasCard = new HasCard(this);
        noCard = new NoCard(this);
        hasCorrectPin = new HasPin(this);
        atmOutOfMoney = new NoCash(this);
        
        atmState = noCard;
        
        if(cashInMachine<0)
        {
          atmState = atmOutOfMoney;
        }
        
        
    }
    
    public void setATMState(ATMState newATMState)
    {
       this.atmState = newATMState;
    }
    
    public void setCashInMachine(int newCashInMachine)
    {
      this.cashInMachine = newCashInMachine;
    }
    
    public void insertCard()
    {
      atmState.insertCard();
    }
    
    public void ejectCard()
    {
      atmState.ejectcard();
    }
    
    public void insertPin(int pinEntered)
    {
      atmState.insertPin(pinEntered);
    }
    public void requestCash(int cashToWithdraw)
    {
      atmState.requestCash(cashToWithdraw);
    }
    
    public ATMState getYesCardState()
    {
      return hasCard;
    }
    
    public ATMState getNoCardState()
    {
      return noCard;
    }
    
    public ATMState getHasPin()
    {
      return hasCorrectPin;
    }
    
    public ATMState getNoCashState()
    {
      return atmOutOfMoney;
    }
    
}
